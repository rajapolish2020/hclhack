const express = require('express');
const router = express.Router();

// declare axios for making http requests
const axios = require('axios');

router.get('/test', (req, res) => {
  res.send('api works');
});

router.post('/user', (req, res) => {
  let data = req && req.body;
  if (data.userId === 'Squad2' && data.password === '123456') {
    res.json({
      status: 'Success',
      data: {
        userId: 19001,
        firstName: 'Polishetti',
        lastName: 'Raja',
        email: 'rajapolish2020@gmail.com',
        password: '123456',
      },
    });
  } else {
    res.json({
      status: 'Error',
      data: {}
    });
  }
});

module.exports = router;
