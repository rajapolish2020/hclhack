var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
const app = express();
const port = 3001;

app.use(bodyParser.json());
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept, Authorization',
  );
  next();
});
var router = express.Router();
// middleware that is specific to this router
// Get our API routes
router.use(function timeLog(req, res, next) {
  console.log('Time: ', Date.now());
  next();
});
const api = require('./routes/api');
// Set our api routes
app.use('/apps/api', api);

app.use('/apps', express.static(path.resolve(__dirname, '../src')));

app.get('/apps/**', (req, res) => {
  res.sendFile(path.resolve(__dirname, '../src/index.html'));
});

app.listen(port, () => console.log(`Server app listening on port ${port}!`));
